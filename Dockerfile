FROM python:3.10

WORKDIR /code
ENV PYTHONPATH="$PYTHONPATH:/code/src"
RUN pip install --upgrade pip

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r requirements.txt

COPY ./src/ /code/src/
COPY ./.env /code/.env

CMD ["uvicorn", "src.inference:app", "--host", "0.0.0.0", "--port", "80"]