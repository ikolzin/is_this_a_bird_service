from typing import Union
from dotenv import load_dotenv
import mlflow
from PIL.Image import Image


class FastAiModel:
    def __init__(self, model_name: str, model_stage: str):
        """
        Initializes an instance of the class with a trained FastAI model from the mlflow registry.

        Args:
            model_name (str): Model name in mlflow registry.
            model_stage (str): Model stage.
        """
        load_dotenv()
        self.learner = mlflow.fastai.load_model(
            model_uri=f"models:/{model_name}/{model_stage}"
        )
        # TODO: right now the model is loaded once, when the service starts. We probably want a script
        # that will periodically check if there is a newer version in MLFlow and use it instead.

    def predict(self, input: Union[Image, bytes, str]) -> dict:
        """
        Predicts the category of an input image using a pre-trained model.

        Args:
            input (Union[Image, bytes, str]): The input image to be classified.

        Returns:
            dict: A dictionary with the classification probabilities for each category.
        """
        categories = self.learner.dls.vocab
        prediction, index, probabilities = self.learner.predict(input)
        classification_result = dict(zip(categories, map(float, probabilities)))
        return classification_result
