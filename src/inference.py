from dotenv import load_dotenv
from fastapi import FastAPI, UploadFile, HTTPException
from models.fastai_model import FastAiModel
import logging
import sys

load_dotenv()
log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=log_fmt)
logger = logging.getLogger(__name__)
logger.info("Initialising application with name 'app'")
app = FastAPI()
logger.info("Loading model")
try:
    model = FastAiModel("bird_levit_384", "Staging")
except Exception as e:
    logger.fatal("Unexpected error while loading model.")
    logger.exception(e)
    sys.exit(1)


@app.post("/invocations")
async def predict(file: UploadFile):
    """
    A POST endpoint with path '/invocations'.
    Receives an image file and returns a prediction.

    Args:
        file (UploadFile): A file-like object containing an image.

    Returns:
        The prediction generated by the model for the given image.

    Raises:
        HTTPException: If the file is not an image file.
    """
    logger.info("Received prediction request")
    # Check if file is an image
    ct = file.content_type
    if ct is not None and ct != "" and ct.split("/")[0] == "image":
        try:
            content = await file.read()
            prediction = model.predict(content)
            logger.info("Done processing.")
            return prediction
        except Exception as e:
            logger.error("Unexpected error while processing the prediction request.")
            logger.exception(e)
            raise HTTPException(
                status_code=500,
                detail="Unexpected error while processing the prediction request.",
            )
    else:
        logger.info("Bad Request")
        # Raise a HTTP 400 Exception - Bad Request
        raise HTTPException(
            status_code=400, detail="Invalid file. Only image files are accepted."
        )
