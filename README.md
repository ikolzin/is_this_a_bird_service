is_this_a_bird_service
==============================

Checks whether the photo is of a bird.

------------

This is a companion project to [is_this_a_bird](https://gitlab.com/ikolzin/is_this_a_bird)
This project provides a way to serve trained models via FastAPI. 